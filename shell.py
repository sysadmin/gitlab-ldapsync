#!/usr/bin/python3
import gitlab
import falcon
import ldap
import yaml
import GitlabLDAPSync
import GitlabLDAPSync.WebEvents

configFile = open( 'settings.yaml', 'r' )
configuration = yaml.safe_load( configFile )

gitlabHost  = configuration['Gitlab']['instance']
gitlabToken = configuration['Gitlab']['token']
gitlabServer = gitlab.Gitlab( gitlabHost, private_token=gitlabToken )

GitlabLDAPSync.WebEvents.configuration = configuration
GitlabLDAPSync.WebEvents.gitlabServer  = gitlabServer
