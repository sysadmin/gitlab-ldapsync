#!/usr/bin/env python
import os
import rq
import sys
import time
import yaml
import redis
import signal
import logging
import argparse
import ldap
import ldap.syncrepl
import ldapurl

# Syncrepl Consumer interface
class SyncClient( ldap.ldapobject.LDAPObject, ldap.syncrepl.SyncreplConsumer ):
	def __init__(self, *args, **kwargs):
		# Initialise the LDAP Connection first
		ldap.ldapobject.LDAPObject.__init__(self, *args, **kwargs)

		# Make a note of whether we have finished the initial replication/sync or not
		self.initialSyncCompleted = False

		# Now prepare the data store
		self.__data = dict()
		# We need this for later internal use
		self.__presentUUIDs = dict()

	# Provides the 'cookie' to the SyncreplConsumer parent class
	# This cookie represents the servers view of our state
	def syncrepl_get_cookie(self):
		if 'cookie' in self.__data:
			return self.__data['cookie']

	# Saves the 'cookie' for the SyncreplConsumer for later retrieval
	def syncrepl_set_cookie(self, cookie):
		self.__data['cookie'] = cookie

	# Per the SyncreplConsumer documentation:
	# Called by syncrepl_poll() between refresh and persist phase.
	# It indicates that initial synchronization is done and persist phase follows.
	#
	# As we are only interested in changes, we ignore the initial 'refresh'
	def syncrepl_refreshdone(self):
		# Mark the refresh phase as completed
		self.initialSyncCompleted = True

		# Make sure any other parent class processing is also completed
		return ldap.syncrepl.SyncreplConsumer.syncrepl_refreshdone(self)

	# Per the SyncreplConsumer documentation:
	# Called by syncrepl_poll() for any added or modified entries.
	#
	# We consider any DN we receive notification of to be something we should act on
	# This is only done of course if the initial sync has been completed!
	def syncrepl_entry(self, dn, attributes, uuid):
		# First we determine the type of change we have here (and store away the previous data for later if needed)
		previous_attributes = dict()
		if uuid in self.__data:
			change_type = 'modify'
			previous_attributes = self.__data[uuid]
		else:
			change_type = 'add'

		# Now we store our knowledge of the existence of this entry (including the DN as an attribute for convenience)
		attributes['dn'] = dn
		self.__data[uuid] = attributes

		# Log something informative
		logging.info('Detected ' + change_type + ' of entry: ' + dn)

		# If the initial sync has been completed, then we need to schedule this change for updating
		if self.initialSyncCompleted:
			self.processChange( dn )

	# Per the SyncreplConsumer documentation:
	# Called by syncrepl_poll() to delete entries. A list of UUIDs of the entries to be deleted is given in the uuids parameter.
	#
	# We pass this notification along to Gitlab so it can act accordingly...
	# (Not sure what it should do though!)
	def syncrepl_delete(self, uuids):
		# Make sure we know about the UUID being deleted, just in case...
		uuids = [uuid for uuid in uuids if uuid in self.__data]

		# Delete all the UUID values we know of
		for uuid in uuids:
			# Schedule an update to Gitlab
			self.processChange( self.__data[ uuid ]['dn'] )

			# Make a note in the log...
			logging.info('Detected deletion of entry: ' + self.__data[uuid]['dn'])

			# Then actually delete it!
			del self.__data[uuid]

	# Per the SyncreplConsumer documentation:
	# Called by syncrepl_poll() whenever entry UUIDs are presented to the client. 
	# syncrepl_present() is given a list of entry UUIDs (uuids) and a flag (refreshDeletes) which indicates whether the server explicitly deleted non-present entries during the refresh operation.
	#
	# We therefore check our internal storage here, and if necessary perform the appropriate deletion
	def syncrepl_present(self, uuids, refreshDeletes = False):
		# If we have not been given any UUID values, then we have recieved all the present controls...
		if uuids is None:
			# We only do things if refreshDeletes is false as the syncrepl extension will call syncrepl_delete instead when it detects a delete notice
			if refreshDeletes is False:
				deletedEntries = [uuid for uuid in self.__data.keys() if uuid not in self.__presentUUIDs and uuid != 'cookie']
				self.syncrepl_delete( deletedEntries )
			# Phase is now completed, reset the list
			self.__presentUUIDs = {}

		else:
			# Note down all the UUIDs we have been sent
			for uuid in uuids:
				self.__presentUUIDs[uuid] = True

	# Process a change detected in the LDAP directory
	# Namely, notify Gitlab of it!
	def processChange(self, dn):
		# Send the notification
		redisQueue.enqueue('GitlabLDAPSync.WebEvents.syncUserUsingLdapDn', dn)

# Shutdown handler
def commenceShutdown(signum, stack):
	# Declare the needed global variables
	global watcher_running, ldap_connection, logging
	logging.info('Recieved signal, shutting down')

	# We are no longer running
	watcher_running = False

	# Tear down the server connection
	if( ldap_connection ):
		del ldap_connection

	# Shutdown
	sys.exit(0)

### Actual Program Begins

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='LDAP monitor to watch for changes and propagate them to Gitlab')
parser.add_argument('--config', help='Path to the configuration file to work with', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.config ):
	print("Unable to locate specified configuration file: %s".format(args.config))
	sys.exit(1)

# Read in our configuration
configFile = open( args.config, 'r' )
configuration = yaml.safe_load( configFile )

# Setup logging
logging.basicConfig( format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO )

# Connect to the Redis Server
redisSocket = configuration['Webservice']['redis-socket']
redisServer = redis.Redis( unix_socket_path=redisSocket )

# Now bring the Redis Queue online - for us to push events into
queueName   = configuration['Webservice']['queue-name']
redisQueue  = rq.Queue( queueName, connection=redisServer )

# Establish some global state for LDAP
watcher_running = True
ldap_connection = False

# Time to actually begin execution
# Install our signal handlers
signal.signal(signal.SIGTERM, commenceShutdown)
signal.signal(signal.SIGINT, commenceShutdown)

# Construct our LDAP Url
ldapHostnamePort = "{}:{}".format( configuration['LDAP']['hostname'], configuration['LDAP']['port'] ) 
ldapServerUrl = ldapurl.LDAPUrl( urlscheme='ldap', hostport=ldapHostnamePort )

# Startup completed, commence running
while watcher_running:
	# Note that we have started trying to reconnect...
	logging.info('Connecting to LDAP server')

	# Prepare the LDAP server connection (triggers the connection as well)
	ldap_connection = SyncClient( ldapServerUrl.initializeUrl() )

	# Now we login to the LDAP server
	# If our credentials are not accepted, then we bail
	# However if instead the connection just fails, then we go around and try again (in 5 seconds)
	try:
		ldap_connection.simple_bind_s( configuration['LDAP']['bind-dn'], configuration['LDAP']['password'] )

	except ldap.INVALID_CREDENTIALS as e:
		logging.critical('Login to LDAP server failed: ' + str(e))
		sys.exit(1)

	except ldap.SERVER_DOWN:
		logging.warning('LDAP server is down')
		time.sleep(5)
		continue

	# Setup our initial persistent search, which will retrieve the entire directory...
	logging.info('Commencing sync process')
	ldap_search = ldap_connection.syncrepl_search( configuration['LDAP']['base'], ldap.SCOPE_SUBTREE, mode='refreshAndPersist', filterstr='(objectClass=*)' )

	# Make sure we continue to check for changes....
	try:
		while ldap_connection.syncrepl_poll( all=1, msgid=ldap_search ):
			pass

	# If we are interrupted though...
	# Maybe the user has asked us to exit?
	except KeyboardInterrupt:
		commenceShutdown()
		pass

	# Something went wrong with the LDAP connection, trigger a go-around
	except Exception as e:
		if watcher_running:
			logging.critical('Encountered a problem, going to retry. Error: ' + str(e))
			time.sleep(5)
		pass
