#!/usr/bin/python3
import rq
import yaml
import redis
import argparse

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Helper to force an account sync')
parser.add_argument('--account', required=True)
args = parser.parse_args()

# Load settings
configFile = open( 'settings.yaml', 'r' )
configuration = yaml.safe_load( configFile )

# Connect to the Redis Server
redisSocket = configuration['Webservice']['redis-socket']
redisServer = redis.Redis( unix_socket_path=redisSocket )

# Now bring the Redis Queue online
queueName   = configuration['Webservice']['queue-name']
redisQueue  = rq.Queue( queueName, connection=redisServer )

redisQueue.enqueue('GitlabLDAPSync.WebEvents.syncUserUsingGitlabUsername', args.account)
