import ldap3
import gitlab

# This isn't the cleanest way to do things, but we don't have another way of passing state around unfortunately
configuration = None
gitlabServer = None

# Updates a given user as requested (using their Gitlab Uid)
def syncUserUsingGitlabUsername( gitlabUsername ):
	# First thing we need to do is retrieve the user from Gitlab
	# We user the list method to ensure that an invalid username doesn't break things
	knownUsers = gitlabServer.users.list( username=gitlabUsername )

	# Make sure we have found a valid user account
	# If we have more than one result, then something has gone terribly wrong, so we do nothing in that instance
	if not knownUsers or len(knownUsers) > 1:
		return True

	# Pull the Gitlab user out of our search results now that we know they exist
	gitlabUser = knownUsers[0]
	ldapUser   = None

	# Now check to see if they're an LDAP user (otherwise we shouldn't search for them)
	# Unfortunately the only way to do this is by iterating over the list of identities attached to the user, looking for our one
	for identity in gitlabUser.identities:
		# Is it the one we are interested in?
		# If not then we should skip it
		if identity['provider'] != configuration['Gitlab']['ldap-provider-name']:
			continue

		# Retrieve the LDAP entry for this Gitlab user...
		ldapConnection = connectToLdapServer()
		ldapConnection.search( identity['extern_uid'], '(objectClass=*)', attributes=['cn', 'mail', 'secondaryMail', 'groupMember'] )
		ldapUser = ldapConnection.entries[0]

	# Did we find an LDAP entry?
	if ldapUser is None:
		return True

	# Perform the sync
	return syncGitlabUser( gitlabUser, ldapUser )

# Updates a given user as requested (using their LDAP DN)
def syncUserUsingLdapDn( userLdapDn ):
	# First thing we need to do is check with Gitlab to see if this user has logged into it
	# We do this by searching for the given LDAP DN
	knownUsers = gitlabServer.users.list( extern_uid=userLdapDn, provider=configuration['Gitlab']['ldap-provider-name'] )

	# Make sure we have found a valid user account (as the user may not have logged into LDAP)
	# If we have more than one result, then something has gone terribly wrong, so we do nothing in that instance
	if not knownUsers or len(knownUsers) > 1:
		return True

	# Pull the Gitlab user out of our search results now that we know they exist
	gitlabUser = knownUsers[0]

	# Now that we know the user has logged into Gitlab, we should retrieve their profile from LDAP
	# For this we have to perform a search and take the first result
	ldapConnection = connectToLdapServer()
	ldapConnection.search( userLdapDn, '(objectClass=*)', attributes=['cn', 'mail', 'secondaryMail', 'groupMember'] )
	ldapUser = ldapConnection.entries[0]

	# Perform the sync
	return syncGitlabUser( gitlabUser, ldapUser )

# Connects to the LDAP server specified in our settings
def connectToLdapServer():
    # Specify the server details...
    ldapServer = ldap3.Server(configuration['LDAP']['hostname'], port=configuration['LDAP']['port'], get_info=ldap3.ALL)
    # Now connect to it
    return ldap3.Connection(ldapServer, configuration['LDAP']['bind-dn'], configuration['LDAP']['password'], auto_bind=True)

# Updates a given Gitlab User based on the Gitlab User and LDAP Object provided
def syncGitlabUser( gitlabUser, ldapUser ):
	# Sync their profile details
	# We don't do their email address as Gitlab will take care of doing that itself
	# For now this means we just do their name, but if we need to do something additional in the future it should be straight forward enough to do
	gitlabUser.name = ldapUser.cn.value
	gitlabUser.save()

	# Next up we synchronise the secondary email addresses for this user
	# To start we'll first assemble a list of emails addresses known to Gitlab
	# For ease of reference, we also prepare a list of addresses that is a bit more easier to check
	gitlabSecondaryEmails = gitlabUser.emails.list()
	emailsKnownToGitlab   = [ emailEntry.email for emailEntry in gitlabSecondaryEmails ] + [ gitlabUser.email ]

	# Assemble an equivalent list of emails known to LDAP as well
	# Because Gitlab always stores emails in lower case form, we need to apply the same sanitization here
	emailsKnownToLdap   = [ address.lower() for address in ldapUser.secondaryMail ] + [ ldapUser.mail.value.lower() ]

	# First, lets remove any address on Gitlab which are no longer known to LDAP
	for address in gitlabSecondaryEmails:
		# Is this address one of the users existing addresses in LDAP?
		# It can be either either primary or secondary (as Gitlab will sort out the primary/secondary status when the user logs in
		if address.email in emailsKnownToLdap:
			continue

		# In that case, this address is no longer valid for the user, and needs to be removed
		address.delete()
		# Remove it from our internal list as well
		emailsKnownToGitlab.remove( address.email )

	# Now we should add any address which is known to LDAP but not in Gitlab
	# Go over all of the addresses and start checking them
	for address in emailsKnownToLdap:
		# Is the address known to Gitlab?
		if address in emailsKnownToGitlab:
			# Then we don't need to do anything further
			continue

		# As it isn't known, we need to add the address to the user
		# We skip confirmation for this as anything in LDAP is assumed to be verified already
		gitlabUser.emails.create( { 'email': address, 'skip_confirmation': True } )

	# With all the profile details synced, now it is time to tackle group memberships
	# For this, we go over each mapping that been defined in turn and check it as needed
	# Any group on Gitlab that does not have a mapping defined here is ignored for the purposes of this sync
	for mapping in configuration['Mapping']:
		# Retrieve the Gitlab group we will be working with
		gitlabGroup = gitlabServer.groups.get( mapping['gitlab-path'] )

		# Determine if the user we are syncing is a current member of the group
		listOfMembers    = gitlabGroup.members.list( user_ids={ gitlabUser.id } )
		gitlabMembership = None
		if len(listOfMembers) > 0:
			gitlabMembership = listOfMembers[0]

		# Do we have anything to do here?
		# If the user is not a member of the relevant group in LDAP and isn't a member in Gitlab, then there is nothing for us to do
		if mapping['ldap-group-name'] not in ldapUser.groupMember and gitlabMembership is None:
			continue

		# First thing we may need to take action on - do we need to add the user to the group?
		# This is the case if the user is a member in LDAP but not on Gitlab
		if mapping['ldap-group-name'] in ldapUser.groupMember and gitlabMembership is None:
			# Add the user to the group
			gitlabGroup.members.create({ 'user_id': gitlabUser.id, 'access_level': mapping['gitlab-access-level'] })
			continue

		# Next thing to check is whether the user is no longer a member in LDAP but is on Gitlab
		# If that is the case, then we should remove their Gitlab group membership to match
		if mapping['ldap-group-name'] not in ldapUser.groupMember and gitlabMembership is not None:
			# Remove the user from the group
			gitlabMembership.delete()
			continue

		# Last thing we need to check is that the correct access level has been applied
		if gitlabMembership.access_level != mapping['gitlab-access-level']:
			# Update the access level
			gitlabMembership.access_level = mapping['gitlab-access-level']
			gitlabMembership.save()

		# All is well for this group mapping
		continue

	# With the user profile synced, and their group memberships synced, there is nothing else left for us to do
	return True
