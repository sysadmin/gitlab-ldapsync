import json
import falcon

# Falcon Resource handler for Gitlab System Hook events
class SystemHookResource(object):

	# Initial Setup
	def __init__(self, redisQueue, gitlabToken):
		# Store everything we need to operate
		self.redisQueue  = redisQueue
		self.gitlabToken = gitlabToken

	# Receive a submission from Gitlab
	def on_post(self, req, resp):
		# First check to make sure the submission comes from Gitlab
		if req.get_header('X-Gitlab-Token') != self.gitlabToken:
			raise falcon.HTTPForbidden('Your request is not valid.')

		# Now that we know we are dealing with a Gitlab System Hook submission, we can decode it
		# We expect to receive a JSON formatted payload
		try:
			body = req.stream.read(req.content_length or 0)
			print(body)
			systemEvent = json.loads(body.decode('utf-8'))
		except:
			raise falcon.HTTPBadRequest('Invalid Submission', 'Valid JSON documents are required.')

		# Do we have a user being created?
		if 'event_name' in systemEvent and systemEvent['event_name'] in ['user_create']:
			# Trigger an update for this specific user
			self.redisQueue.enqueue('GitlabLDAPSync.WebEvents.syncUserUsingGitlabUsername', systemEvent['username'])

		# Perhaps someone has gained/lost access to a project?
		if 'event_name' in systemEvent and systemEvent['event_name'] in ['user_add_to_group', 'user_remove_from_group', 'user_update_for_group']:
			# To ensure someone can't be added to a managed group or have additional access granted, we forcibly sync their access now
			self.redisQueue.enqueue('GitlabLDAPSync.WebEvents.syncUserUsingGitlabUsername', systemEvent['user_username'])

		# Prepare our response
		jsonResponse = {'message': 'OK'}

		# Give Gitlab the all clear that we've done what is needed
		resp.status = falcon.HTTP_200
		resp.body = json.dumps(jsonResponse)
